const express = require("express");
const router = express.Router();

const { delCreate, getFormMultiply, getFormTrueFalse, cancelTakeTest } = require('../controllers/cardExpal.controller');
const {checkToken} = require('../middleware/auth.middleware');

router.post('/getFormMultiply', checkToken, getFormMultiply);
router.post('/getFormTrueFalse', checkToken, getFormTrueFalse);
router.delete('/cancelTakeTest/:id', checkToken, cancelTakeTest);

router.post('/delCreate', delCreate);

module.exports = router;