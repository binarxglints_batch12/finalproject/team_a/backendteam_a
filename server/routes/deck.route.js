const express = require("express");
const router = express.Router();

const { getDeckLimit, getAllDecks, getDeckAll, updateDeck, deleteDecks, getSearchByIdCategory,createDeck, getCategory, sortDeck, getDeckByUserId, findDecksId} = require('../controllers/deck.controller')
const {checkToken, validateCreateorUpdateDeck} = require('../middleware/auth.middleware');

router.get('/AllDecks', checkToken, getAllDecks);
router.get('/myDeck', checkToken, getDeckByUserId);
router.get('/getDeck/:id', checkToken, findDecksId);
router.get('/getDeckLimit',checkToken, getDeckLimit);
router.get('/getCategory', getCategory);
router.post('/createDeck', validateCreateorUpdateDeck, checkToken, createDeck);
router.get('/getDeckAll/:id', checkToken, getDeckAll);
router.post("/getDeckByCategory", checkToken, getSearchByIdCategory);
router.put("/updateDeck/:id", validateCreateorUpdateDeck, checkToken, updateDeck);
router.delete("/deleteDeck/:id", checkToken, deleteDecks);
router.post('/getSortDecks', checkToken, sortDeck);

module.exports = router;