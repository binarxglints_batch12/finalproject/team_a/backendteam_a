const mongoose = require("mongoose");

const cardModel = mongoose.model(
  'card',
  mongoose.Schema(
      {
        decksId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'deck'
        },
        term: {
            type: String
        },
        imageExplanation: {
            type: String
        },
        explanation: {
            type: String
        }
      },
      { timestamps: false }
  )
)

module.exports = cardModel;