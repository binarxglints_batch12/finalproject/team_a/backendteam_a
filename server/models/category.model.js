const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const categoryModel = mongoose.model(
  'category',
  mongoose.Schema(
      {
          label: {
            type: String,
          }
      },
      { timestamps: false }
  )
)

module.exports = categoryModel;