const mongoose = require("mongoose");

const decksTest = mongoose.model(
  'decksTest',
  mongoose.Schema(
      {
        userId: {
            type: mongoose.Schema.Types.ObjectId
        },
        deckId: {
            type: mongoose.Schema.Types.ObjectId
        },
        score: {
            type: Number
        },
        true: {
            type: Number
        },
        false: {
            type: Number
        }
      },
      { timestamps: false }
  )
)

module.exports = decksTest;