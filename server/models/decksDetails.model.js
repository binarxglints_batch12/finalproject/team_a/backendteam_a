const mongoose = require("mongoose");

const decksDetailsModel = mongoose.model(
  'decksDetails',
  mongoose.Schema(
      {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        },
        decksId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'deck'
        },
        progress: {
            type: Number
        },
        true: {
            type: Number
        },
        false: {
            type: Number
        },
        notStudied: {
            type: Number
        }
      },
      { timestamps: false }
  )
)

// const newDeck = new decksDetailsModel({
//     userId: "60dad9259fa77e16edc9ed64",
//     decksId: "60e68260827c02000891771d",
//     progress: 60,
//     true: 4,
//     false: 2
// })
// newDeck.save()



module.exports = decksDetailsModel;