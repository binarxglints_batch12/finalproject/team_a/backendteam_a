require('dotenv').config();
const express = require('express');
const cors = require('cors');

var app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const authRoutes = require('./routes/auth.route');
const deckRoutes = require('./routes/deck.route');

const profileRoutes = require('./routes/profile.route');
const cardRoutes = require('./routes/card.route');
const decksDetailsRoutes = require('./routes/decksdetails.route');
const cardExpalRoutes = require('./routes/cardExpal.route');
const takeTestRoutes = require('./routes/take_test.route');
const practiceRoutes = require('./routes/practiceTest.route')

app.use('/api', 
    authRoutes, 
    deckRoutes, 
    profileRoutes, 
    cardRoutes, 
    decksDetailsRoutes,
    cardExpalRoutes,
    takeTestRoutes,
    practiceRoutes
);


app.get('/', (req, res) =>  {
    res.send('Welcome to Server Deck-o API ');
});

app.all('*', (req, res) => {
    res.send('Are you lost...?');
});

const PORT = process.env.PORT || 3000;
require("./config/config")();

const server = app.listen(PORT, () => {
    console.log('server up on port %s...', server.address().port);
});