const { User } = require('../models');
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const method = {};

method.postRegistrasi = async (req, res) => {
    try {
        const dtEmail = req.body.email;
        const email = dtEmail.toLowerCase()
        if(email){
            const cekEmail = await User.findOne({ email });
            if(!cekEmail){
                const dtUser = new User(
                    {
                        email: email,
                        password: req.body.password,
                        username: req.body.username
                    }
                );

                await dtUser.save();

                const token = jwt.sign({
                    id: dtUser.id,
                    email: dtUser.email,
                    username: dtUser.username
                }, process.env.SECRET_KEY);

                const dtUserRegis = {
                    id: dtUser.id,
                    username: dtUser.username,
                    email: dtUser.email
                }

                res.header('Authorization', token);
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'Registration successfully.',
                    data: dtUserRegis,
                    token: token
                });
            } else {
                res.status(400).send({
                    statusCode: 400,
                    message: 'Email already registered.'
                });
            }
        }
    } catch (error) {
        res.status(500).send(error);
    }
}


method.postLogin = async (req, res) => {
    try {
        const { email, password } = req.body;
        const emailLower = email.toLowerCase()
        const dtUser = await User.findOne({ email: emailLower });
        if(dtUser){
            const verify = await bcrypt.compare(password, dtUser.password);
            if(verify === true){
                const token = jwt.sign({
                    id: dtUser.id,
                    email: dtUser.email,
                    username: dtUser.username
                }, process.env.SECRET_KEY);

                const dtUserLogin = {
                    id: dtUser.id,
                    username: dtUser.username,
                    email: dtUser.email
                }

                res.header('Authorization', token);
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'Login successfully.',
                    data: dtUserLogin,
                    token: token
                });
            } else {
                res.status(404).send({
                    statusCode: 404,
                    message: 'Wrong password!'
                });
            }
        } else {
            res.status(404).send({
                statusCode: 404,
                message: 'Email not registered, Please create an account first.'
            });
        }

    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = method;
