const { User } = require('../models');
const bcrypt = require('bcrypt');
const method = {};

method.getProfile = async (req, res) => {
    try {
        const token = req.token;
        const idProfile = token.id;
        
        const dtProfile = await User.findById(idProfile);

        const dataRes = {
            username: dtProfile.username,
            email: dtProfile.email
        }

        res.status(200).json({
            statusCode: 200,
            statusText: 'success',
            message: " Your request for Get profile successfully",
            data: dataRes
        });

    } catch (error) {
        res.status(500).send(error);
    }
}

method.putProfile = async (req, res) => {
    try {
        const token = req.token;
        const idProfile = token.id;

        const dtProfile = await User.findById(idProfile);

        if(dtProfile){
            const {username, email} = req.body;
            const emailLower = email.toLowerCase()
            const checkEmail = await User.findOne({ email: emailLower });

            if(checkEmail){
                if(dtProfile.id === checkEmail.id){

                const dtUpdate = {
                    username: req.body.username,
                    email: emailLower
                }

                dtProfile.set(dtUpdate);
                await dtProfile.save();

                const dtRes = {
                    username: username,
                    email: emailLower
                }

                res.status(200).json({
                    statusCode: 200,
                    statusText: "success",
                    message: " Your request for update profile successfully",
                    data: dtRes
                });
                } else {
                    res.status(400).send({
                        statusCode: 400,
                        message: 'Email already registered.'
                    });
                }
            } else {
                const dtUpdate = {
                    username: req.body.username,
                    email: emailLower
                }

                dtProfile.set(dtUpdate);
                await dtProfile.save();

                const dtRes = {
                    username: username,
                    email: emailLower
                }

                res.status(200).json({
                    statusCode: 200,
                    statusText: "success",
                    message: " Your request for update profile successfully",
                    data: dtRes
                });
            }
        }

    } catch (error) {
        res.status(500).send(error);
    }
}

method.putPassword = async (req, res) => {
    const token = req.token;
    const idProfile = token.id;

    const {current_password, password} = req.body;

    const dtProfile = await User.findById(idProfile);

    const verify = await bcrypt.compare(current_password, dtProfile.password);
    if(verify === true){
        const dtUpdate = {
            password: password
        }

        dtProfile.set(dtUpdate);
        await dtProfile.save();

        res.status(200).json({
            statusCode: 200,
            statusText: "success",
            message: " Your request for update password successfully",
        });

    } else {
        res.status(400).send({
            statusCode: 400,
            message: 'The current password is not the same.'
        });
    }
}

module.exports = method;