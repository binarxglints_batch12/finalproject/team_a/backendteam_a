# BackendTeam_A #Deck-O

# API List

| Routes | EndPoint                        | Description                                                   |
| ------ | ------------------------------- | ------------------------------------------------------------- |
| POST   | /api/register                   | Register members                                              |
| POST   | /api/login                      | Get Token & Login members with (username, email and password) |
| GET    | /api/getProfil                  | get member by token to get id                                 |
| PUT    | /api/putProfile                 | update Profile member by token to get id                      |
| PUT    | /api/putPassword                | update Password member by token to get id                     |
| POST   | /api/createDeck                 | to Add Deck                                                   |
| GET    | /api/getDeck/:id                | Get deck by id(/:id) and same user                            |
| GET    | /api/getDeckAll/2               | to get all deck create by user (page & size)                  |
| PUT    | /api/updateDeck/:id             | to Update Deck by id (/:id) and same user                     |
| DELETE | /api//deleteDeck/:id            | to Delete Deck by id(/:id) and same user                      |
| GET    | /api/getCard/:decksId           | to Get Card by DecksId (/:decksId)                            |
| POST   | /api/createCard                 | to Add Card                                                   |
| PUT    | /api/updateCard/:id             | to Update Card by id (/:id)                                   |
| POST   | /api/deleteCard/:id             | to Delete Card by id (/:id)                                   |
| DELETE | /api/rMovie/delete/reviews/:id  | Delete Review and rating by id (USER_ONLY !)                  |
| GET    | /api/getDecksDetailsAllByUserID | to get All Decks have practice test by user                   |
| GET    | /api/getDeckLimit               | to Get All Decks have been create by user limit 4             |
| GET    | /api/practiceTest/:id           | to Take Practice Test by id deck (/:id)                       |
| POST   | /api/swipeLeft                  | to Swipe Left when user don't know answer this question       |
| POST   | /api/swipeRight                 | to Swipe Right when user know answer this question            |
| GET    | /api/recall/:id                 | to get card need to recall by deckId (/:id)                   |
| GET    | /api/mastered/:id               | to get card mastered by deckId (/:id)                         |
| GET    | /api/getFormMultiply            | to Take test for multiple choice questions                    |
| GET    | /api/getFormTrueFalse           | to Take test for True False choice questions                  |
| DELETE | /api/cancelTakeTest/:id         | to Cancel Take test by id (/:id)                              |
| GET    | /api/getResultTakeTest/:id      | to Get Result Take test by id (/:id)                          |
| PUT    | /api/RetakeTheTest/:id          | to Retake Take test by id (/:id)                              |
| POST   | /api/getDeckByCategory          | to Filter Decks by Category                                   |
| GET    | /api/getCategory                | to Get All Category                                           |
| POST   | /api/getSortDecks               | to Sort Decks                                                 |
| GET    | /api/myDeck                     | Show All Deck user create                                     |

---

# DEVELOPER :

```
$ Amalia Putri Yulandi      (Backend Leader)
$ Ahkam Alman Syah Putra    (Backend Co-Leader)
$ Harry Hermanan            (Scrum Master, Developer)
```

# How to server

```bash
$ cd server
$ npm i
$ npm start
```
